const express = require("express");
const {
  getAllProducts,
  getsingleProduct,
  addProduct,
  updateProductState,
} = require("../controllers/products");

const productRouter = express.Router();

productRouter.route("/").get(getAllProducts).post(addProduct);
productRouter.route("/:id").get(getsingleProduct).put(updateProductState);

// router.get("/productcategory/:id", getProductCategory);
// router.post("/productcategory", addCategory);

module.exports = productRouter;
