const ErrorResponse = require("../utils/errorResponse");
const asyncHandler = require("../middleware/async");
const Product = require("../models/Product");
const { OK, BAD_REQUEST } = require("http-status-codes");

// @desc      Get getAllProducts
// @route     GET /api/v1/products
// @access    Public
exports.getAllProducts = asyncHandler(async (req, res, next) => {
  const products = await Product.find({ productstate: "available" }).exec();

  res.status(200).json({
    success: true,
    count: products.length,
    data: products,
  });
});

// @desc      Create new product
// @route     POST /api/v1/product
// @access    puplic
exports.addProduct = asyncHandler(async (req, res, next) => {
  try {
    const product = await Product.create(req.body);

    res.status(201).json({
      success: true,
      data: product,
    });
  } catch (error) {
    return next(new ErrorResponse(`${error}`, 404));
  }
});

exports.getsingleProduct = asyncHandler(async (req, res, next) => {
  const product = await Product.findById(req.params.id).exec();

  if (!product) {
    return next(
      new ErrorResponse(`No Product with the id of ${req.params.id}`),
      404
    );
  }

  res.status(200).json({ success: true, data: product });
});

exports.updateProductState = asyncHandler(async (req, res, next) => {
  const { productstate } = req.body;
  let product = await Product.findById(req.params.id).exec();
  if (!product) {
    return next(
      new ErrorResponse(`No Product with the id of ${req.params.id}`),
      404
    );
  }
  console.log("befour update", product);

  if (product.productstate === "available") {
    product = await Product.findOneAndUpdate(
      { _id: req.params.id },
      { $set: { productstate } },
      { new: true, runValidators: true }
    );

    console.log("after update", product);
    res.status(200).json({ success: true, data: product });
  } else {
    return next(
      new ErrorResponse(
        `it is not accept to change draft to sold or sold to draft`
      ),
      BAD_REQUEST
    );
  }
});
